from gui import Gui

def main() -> None:
    gui = Gui()
    gui.initialize_gui()

main()