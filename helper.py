import os
class Helper:

    def uniquify_before_movement(target_path: str) -> str:
        filename, extension = os.path.splitext(target_path)
        counter = 1

        while(os.path.exists(target_path)):
            target_path = filename + " (" + str(counter) + ")" + extension
            counter += 1
        return target_path
