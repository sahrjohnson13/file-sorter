import os
from tkinter import *
import tkinter as tk
from tkinter import filedialog, messagebox
import file_sorter as fs

class Gui:
    
    def initialize_gui(self):
        def submit_action():
            dir_path = entry.get()

            file_types = ["txt", "zip", "mp4", "pak", "msi", "docx", "png", "rar", "jpg", "pdf", "jpg", "html", "exe", "pptx", "pptx", "odt", "jfif", "7z", "json", "bmp"]
            if dir_path:    
                fs.unpackage_directory(dir_path, "Sorted Extensions")
                for type in file_types:
                    fs.package_by_extension(dir_path, type, type)
                fs.loose_directory_sort(dir_path, file_types, "Loose Folders")
                fs.package_directory(dir_path, file_types, "Sorted Extensions")
                fs.remove_empty_directories(dir_path)
            messagebox.showinfo("Sorted Directory", f"Sorted the following directory by extension: {dir_path}")

        def quit_action():
            root.destroy()

        def browse_file():
            dir_path = filedialog.askdirectory()
            if dir_path:
                entry.delete(0, tk.END)
                entry.insert(0, dir_path)

        root = tk.Tk()
        root.title("File Path Input")

        root.geometry("600x200")
        root.configure(bg="#f0f0f0")

        frame = tk.Frame(root, bg="#f0f0f0", padx=10, pady=10)
        frame.pack(expand=True, fill=tk.BOTH)

        label = tk.Label(frame, text="File Path:", bg="#f0f0f0", font=("Arial", 12))
        label.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)

        entry = tk.Entry(frame, width=40, font=("Arial", 12))
        entry.grid(row=0, column=1, padx=5, pady=5, sticky=tk.W)

        browse_button = tk.Button(frame, text="Browse", command=browse_file, font=("Arial", 12), bg="#4CAF50", fg="white")
        browse_button.grid(row=0, column=2, padx=5, pady=5)

        submit_button = tk.Button(frame, text="Submit", command=submit_action, font=("Arial", 12), bg="#2196F3", fg="white")
        submit_button.grid(row=1, column=0, columnspan=2, padx=5, pady=10, sticky=tk.EW)

        quit_button = tk.Button(frame, text="Quit", command=quit_action, font=("Arial", 12), bg="#f44336", fg="white")
        quit_button.grid(row=1, column=2, padx=5, pady=10, sticky=tk.EW)
        
        root.mainloop()

