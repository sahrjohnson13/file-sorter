import os
import shutil
from helper import Helper
        
def loose_directory_sort(parent_directory, excluded_directories, new_directory_name) -> None:
    source_files = os.listdir(parent_directory)
    path = os.path.join(parent_directory, new_directory_name)

    if (not os.path.exists(path)):
        os.mkdir(path)
    
    for file in source_files:
        if (os.path.isdir(os.path.join(parent_directory, file)) and file not in excluded_directories and file != new_directory_name):
            shutil.move(os.path.join(parent_directory, file), Helper.uniquify_before_movement(os.path.join(path, file)))
        else:
            continue

def package_by_extension(parent_directory, extension, directory_name) -> None:
    source_files = os.listdir(parent_directory)
    path = os.path.join(parent_directory, directory_name)

    if (not os.path.exists(path)):
        os.mkdir(path)

    for file in source_files:
        if file.endswith(f".{extension}"):
            shutil.move(os.path.join(parent_directory, file), Helper.uniquify_before_movement(os.path.join(path, file)))
        else:
            continue

    if len(os.listdir(path)) == 0:
        os.rmdir(path)

def package_directory(parent_directory, directory_names, new_directory_name) -> None:
    source_files = os.listdir(parent_directory)
    path = os.path.join(parent_directory, new_directory_name)
    
    if (not os.path.exists(path)):
        os.mkdir(path)

    for file in source_files:
        if(file in directory_names):
            shutil.move(os.path.join(parent_directory, file), Helper.uniquify_before_movement(os.path.join(path, file)))
        else:
            continue

def unpackage_directory(parent_directory, target_directory_filename) -> None:
    path = os.path.join(parent_directory, target_directory_filename)

    if (os.path.exists(path)):
        for file in os.listdir(path):
            shutil.move(os.path.join(path, file), Helper.uniquify_before_movement(os.path.join(parent_directory, file)))
        os.rmdir(path)

def remove_empty_directories(parent_directory) -> None:
    source_files = os.listdir(parent_directory)

    for file in source_files:
        path = os.path.join(parent_directory, file)
        if (os.path.exists(path) and os.path.isdir(path)):
            if (len(os.listdir(path)) == 0):
                os.rmdir(path)